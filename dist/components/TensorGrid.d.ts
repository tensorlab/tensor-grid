/// <reference types="react" />
import { TensorGridType } from "./types";
export declare const TensorGrid: (props: TensorGridType) => JSX.Element;
