import React, {useState} from "react";
import {TensorPanel, TensorGrid} from "tensor-grid";

export const App = (props: any) => {
  const [grids, setGrids] = useState([
    {x: 0, y: 0, w: 1, h: 1},
    {x: 1, y: 0, w: 1, h: 1},
    {x: 2, y: 0, w: 2, h: 1},
    {x: 0, y: 1, w: 1, h: 1},
    {x: 1, y: 1, w: 2, h: 1},
    {x: 3, y: 1, w: 1, h: 1},
  ])
  return (
    <TensorPanel columns={4} editable={true}>
      {grids.map((grid, idx) => (
        <TensorGrid
          key={idx}
          {...grid}
          onChangeValues={(x, y, w, h) => {
            console.log(x,y,w,h)
            let g = grids
            g.splice(idx, 1, {...grids[idx], x, y, w, h})
            setGrids([...g])
          }}
        >
          <h3>Grid {idx}</h3>
        </TensorGrid>
      ))}
    </TensorPanel>
  )
}
