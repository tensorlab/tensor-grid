import React, {Fragment, useContext, useState} from "react"
import {TensorGridType} from "./types";
import Draggable from "react-draggable"
import {gridContext} from "./contexts";

export const TensorGrid = (props: TensorGridType) => {
  const {x, y, w, h} = props;
  const [tmp, setTmp] = useState({d: false, x, y, w, h})  // temporary drag/resize pos
  const {multiplier, editable} = useContext(gridContext)
  return (
    <Fragment>
      {tmp.d && <Draggable
        disabled={true}
        position={{x: tmp.x * multiplier, y: tmp.y * multiplier}}
      >
        <div className="tensor-grid placeholder" style={{width: tmp.w * multiplier, height: tmp.h * multiplier}}>
          <div className="tensor-grid-card"/>
        </div>
      </Draggable>}
      <Draggable
        disabled={!editable}
        position={{x: props.x * multiplier, y: props.y * multiplier}}
        onStart={(e, data) => {
          setTmp({...tmp, d: true, x, y})
        }}
        onDrag={((e, data) => {
          let x = Math.floor((data.x + multiplier/2) / multiplier)
          let y = Math.floor((data.y + multiplier/2) / multiplier)
          setTmp({...tmp, x, y})
        })}
        onStop={(e, data) => {
          setTmp({...tmp, d: false})
          let x = Math.floor((data.x + multiplier/2) / multiplier)
          let y = Math.floor((data.y + multiplier/2) / multiplier)
          props.onChangeValues?.(x, y, tmp.w, tmp.h)
        }}
      >
        <div className={`tensor-grid${tmp.d ? " dragging" : ''}`}
             style={{width: props.w * multiplier, height: props.h * multiplier}}>
          <div className="tensor-grid-card">
            {props.children}
          </div>
        </div>
      </Draggable>
    </Fragment>

  )
}
