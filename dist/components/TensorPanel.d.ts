/// <reference types="react" />
import { TensorPanelType } from "./types";
export declare const TensorPanel: (props: TensorPanelType) => JSX.Element;
