import React, { createContext, useState, useEffect, useContext, Fragment } from 'react';
import Draggable from 'react-draggable';

const gridContext = createContext({
  multiplier: 200,
  editable: true
});

const TensorPanel = props => {
  const [multiplier, setMultiplier] = useState(window.innerWidth > 767 ? (window.innerWidth - 30) / props.columns : window.innerWidth - 30);
  const [sm, setSm] = useState(window.innerWidth < 768);
  console.log(props.columns);

  const handleResize = () => {
    let width = window.innerWidth;
    setMultiplier(width > 767 ? (width - 30) / props.columns : width - 30);
    setSm(width < 768);
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [multiplier]);
  return React.createElement(gridContext.Provider, {
    value: {
      multiplier: multiplier,
      editable: props.editable === undefined ? true : props.editable
    }
  }, React.createElement("div", {
    className: "tensor-panel"
  }, props.children));
};

const TensorGrid = props => {
  const {
    x,
    y,
    w,
    h
  } = props;
  const [tmp, setTmp] = useState({
    d: false,
    x,
    y,
    w,
    h
  });
  const {
    multiplier,
    editable
  } = useContext(gridContext);
  return React.createElement(Fragment, null, tmp.d && React.createElement(Draggable, {
    disabled: true,
    position: {
      x: tmp.x * multiplier,
      y: tmp.y * multiplier
    }
  }, React.createElement("div", {
    className: "tensor-grid placeholder",
    style: {
      width: tmp.w * multiplier,
      height: tmp.h * multiplier
    }
  }, React.createElement("div", {
    className: "tensor-grid-card"
  }))), React.createElement(Draggable, {
    disabled: !editable,
    position: {
      x: props.x * multiplier,
      y: props.y * multiplier
    },
    onStart: (e, data) => {
      setTmp({ ...tmp,
        d: true,
        x,
        y
      });
    },
    onDrag: (e, data) => {
      let x = Math.floor((data.x + multiplier / 2) / multiplier);
      let y = Math.floor((data.y + multiplier / 2) / multiplier);
      setTmp({ ...tmp,
        x,
        y
      });
    },
    onStop: (e, data) => {
      var _props$onChangeValues;

      setTmp({ ...tmp,
        d: false
      });
      let x = Math.floor((data.x + multiplier / 2) / multiplier);
      let y = Math.floor((data.y + multiplier / 2) / multiplier);
      (_props$onChangeValues = props.onChangeValues) === null || _props$onChangeValues === void 0 ? void 0 : _props$onChangeValues.call(props, x, y, tmp.w, tmp.h);
    }
  }, React.createElement("div", {
    className: `tensor-grid${tmp.d ? " dragging" : ''}`,
    style: {
      width: props.w * multiplier,
      height: props.h * multiplier
    }
  }, React.createElement("div", {
    className: "tensor-grid-card"
  }, props.children))));
};

export { TensorGrid, TensorPanel };
//# sourceMappingURL=index.modern.js.map
