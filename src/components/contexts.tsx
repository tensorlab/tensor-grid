import {createContext} from "react";

export const gridContext = createContext({
  multiplier: 200,
  editable: true
})
