# tensor-grid

> A React Library for creating editable grid panel

[![NPM](https://img.shields.io/npm/v/gridpanel.svg)](https://www.npmjs.com/package/gridpanel) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

Using yarn

```bash
yarn add tensor-grid
```

Using npm

```bash
npm install --save tensor-grid
```

## Usage

```tsx
import React, {Component} from 'react'

import {TensorPanel, TensorGrid} from "tensor-grid";
import 'tensor-grid/dist/tensor-grid.css'

class Example extends Component {
  render() {
    return (
      <TensorPanel columns={4} editable={true}>
        <TensorGrid x={0} y={0} w={1} h={1}>
          Your Content Here
        </TensorGrid>
        <TensorGrid x={1} y={0} w={2} h={1}>
          Your Content Here
        </TensorGrid>
      </TensorPanel>
    )
  }
}
```

## License

MIT © [ghimiresdp](https://github.com/ghimiresdp)
