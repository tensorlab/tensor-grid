import { HTMLAttributes } from "react";
export interface TensorPanelType extends HTMLAttributes<any> {
    columns: number;
    rows?: number;
    width?: number | string;
    editable?: boolean;
}
export interface TensorGridType extends HTMLAttributes<any> {
    x: number;
    y: number;
    w: number;
    h: number;
    onChangeValues?(x: number, y: number, w: number, h: number): void;
}
