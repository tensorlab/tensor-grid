import { GridPanelType } from "./types";
export declare const Panel: (props: GridPanelType) => JSX.Element;
