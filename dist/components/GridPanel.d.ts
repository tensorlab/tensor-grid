import { GridPanelType } from "./types";
export declare const GridPanel: (props: GridPanelType) => JSX.Element;
