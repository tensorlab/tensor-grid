import React, {useEffect, useState} from "react";
import {gridContext} from "./contexts";
import {TensorPanelType} from "./types";

export const TensorPanel = (props: TensorPanelType) => {
  const [multiplier, setMultiplier] = useState((window.innerWidth > 767) ? ((window.innerWidth - 30) / props.columns) : (window.innerWidth - 30));
  const [sm, setSm] = useState(window.innerWidth < 768);
  console.log(props.columns);


  const handleResize = () => {
    let width = window.innerWidth;
    setMultiplier((width > 767) ? ((width - 30) / props.columns) : (width - 30));
    setSm(width < 768);
  };
  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [multiplier]);
  return (
    <gridContext.Provider value={{multiplier: multiplier, editable: props.editable===undefined?true:props.editable}}>
      <div className="tensor-panel">
        {props.children}
      </div>
    </gridContext.Provider>
  );
};
