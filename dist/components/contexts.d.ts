/// <reference types="react" />
export declare const gridContext: import("react").Context<{
    multiplier: number;
    editable: boolean;
}>;
