import React from "react";
import ReactDOM from "react-dom";
import {App} from "./App";
import "tensor-grid/src/scss/style.scss"


ReactDOM.render(<App/>, document.querySelector("div#root"));
