function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var Draggable = _interopDefault(require('react-draggable'));

var gridContext = React.createContext({
  multiplier: 200,
  editable: true
});

var TensorPanel = function TensorPanel(props) {
  var _useState = React.useState(window.innerWidth > 767 ? (window.innerWidth - 30) / props.columns : window.innerWidth - 30),
      multiplier = _useState[0],
      setMultiplier = _useState[1];

  var _useState2 = React.useState(window.innerWidth < 768),
      setSm = _useState2[1];

  console.log(props.columns);

  var handleResize = function handleResize() {
    var width = window.innerWidth;
    setMultiplier(width > 767 ? (width - 30) / props.columns : width - 30);
    setSm(width < 768);
  };

  React.useEffect(function () {
    window.addEventListener("resize", handleResize);
    return function () {
      window.removeEventListener("resize", handleResize);
    };
  }, [multiplier]);
  return React__default.createElement(gridContext.Provider, {
    value: {
      multiplier: multiplier,
      editable: props.editable === undefined ? true : props.editable
    }
  }, React__default.createElement("div", {
    className: "tensor-panel"
  }, props.children));
};

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var TensorGrid = function TensorGrid(props) {
  var x = props.x,
      y = props.y,
      w = props.w,
      h = props.h;

  var _useState = React.useState({
    d: false,
    x: x,
    y: y,
    w: w,
    h: h
  }),
      tmp = _useState[0],
      setTmp = _useState[1];

  var _useContext = React.useContext(gridContext),
      multiplier = _useContext.multiplier,
      editable = _useContext.editable;

  return React__default.createElement(React.Fragment, null, tmp.d && React__default.createElement(Draggable, {
    disabled: true,
    position: {
      x: tmp.x * multiplier,
      y: tmp.y * multiplier
    }
  }, React__default.createElement("div", {
    className: "tensor-grid placeholder",
    style: {
      width: tmp.w * multiplier,
      height: tmp.h * multiplier
    }
  }, React__default.createElement("div", {
    className: "tensor-grid-card"
  }))), React__default.createElement(Draggable, {
    disabled: !editable,
    position: {
      x: props.x * multiplier,
      y: props.y * multiplier
    },
    onStart: function onStart(e, data) {
      setTmp(_extends({}, tmp, {
        d: true,
        x: x,
        y: y
      }));
    },
    onDrag: function onDrag(e, data) {
      var x = Math.floor((data.x + multiplier / 2) / multiplier);
      var y = Math.floor((data.y + multiplier / 2) / multiplier);
      setTmp(_extends({}, tmp, {
        x: x,
        y: y
      }));
    },
    onStop: function onStop(e, data) {
      var _props$onChangeValues;

      setTmp(_extends({}, tmp, {
        d: false
      }));
      var x = Math.floor((data.x + multiplier / 2) / multiplier);
      var y = Math.floor((data.y + multiplier / 2) / multiplier);
      (_props$onChangeValues = props.onChangeValues) === null || _props$onChangeValues === void 0 ? void 0 : _props$onChangeValues.call(props, x, y, tmp.w, tmp.h);
    }
  }, React__default.createElement("div", {
    className: "tensor-grid" + (tmp.d ? " dragging" : ''),
    style: {
      width: props.w * multiplier,
      height: props.h * multiplier
    }
  }, React__default.createElement("div", {
    className: "tensor-grid-card"
  }, props.children))));
};

exports.TensorGrid = TensorGrid;
exports.TensorPanel = TensorPanel;
//# sourceMappingURL=index.js.map
